﻿using common_lib.command.queue;
using unity_common_lib.utils;

namespace unity_common_lib.command.queue
{
    public class UnityCommandQueue : CommandQueue
    {
        public UnityCommandQueue()
        {
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public override void Start()
        {
            if (!_started && _queue.Count > 0 && _currentCommand == null)
            {
                nextCommandInNextFrame();
            }
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected void nextCommandInNextFrame()
        {
            _started = true;
            UnityUtils.ExecuteInNextFrame(nextCommand);

            //DebugUtil.Log("UnityCommandQueue: nextCommandInNextFrame");
        }

        protected override void nextCommand()
        {
            //DebugUtil.Log("UnityCommandQueue: nextCommand");

            CommandQueueItem commandQueueItem = _queue.Dequeue();
            _currentCommand = commandQueueItem.Command;
            if (commandQueueItem.BlockAfterComplete)
                _currentCommand.CommandComplete = this.cleanCommand;
            else
                _currentCommand.CommandComplete = this.onCommandCompleteStartNext;
            _currentCommand.Execute();
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        protected void onCommandCompleteStartNext()
        {
            cleanCommand();
            if (_queue.Count > 0)
            {
                nextCommandInNextFrame();
            }
        }
    }
}