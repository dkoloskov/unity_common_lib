﻿using System;
using System.Collections.Generic;

namespace unity_common_lib
{
    class UnityGlobal
    {
        private static List<Action> updateHandlers;

        static UnityGlobal()
        {
            updateHandlers = new List<Action>();
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        // +++++++++++++++ UPDATE +++++++++++++++
        public static void AddUpdateHandler(Action handler)
        {
            updateHandlers.Add(handler);
        }

        public static void RemoveUpdateHandler(Action handler)
        {
            updateHandlers.Remove(handler);
        }

        public static void Update()
        {
            // TODO: Write array, instead of list
            for(int i=0; i<updateHandlers.Count; i++)
            {
                updateHandlers[i]();
            }
        }
        // --------------- UPDATE ---------------
    }
}