﻿//#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

#pragma warning disable 0414, 0162

namespace unity_common_lib.utils.debug
{
    /// <summary>
    /// Overrides Unity's Debug class because it's in the root namespace.
    /// </summary>
    public static class AsyncDebug
    {

        public static bool developerConsoleVisible { get { return Debug.developerConsoleVisible; } set { Debug.developerConsoleVisible = value; } }
        public static bool isDebugBuild { get { return Debug.isDebugBuild; } }

        public static void Break() { Debug.Break(); }
        public static void ClearDeveloperConsole() { Debug.ClearDeveloperConsole(); }
        public static void DebugBreak() { Debug.DebugBreak(); }
        public static void DrawLine(Vector3 start, Vector3 end) { Debug.DrawLine(start, end); }
        public static void DrawLine(Vector3 start, Vector3 end, Color color) { Debug.DrawLine(start, end, color); }
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration) { Debug.DrawLine(start, end, color, duration); }
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest) { Debug.DrawLine(start, end, color, duration, depthTest); }
        public static void DrawRay(Vector3 start, Vector3 dir) { Debug.DrawRay(start, dir); }
        public static void DrawRay(Vector3 start, Vector3 dir, Color color) { Debug.DrawRay(start, dir, color); }
        public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration) { Debug.DrawRay(start, dir, color, duration); }
        public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration, bool depthTest) { Debug.DrawRay(start, dir, color, duration, depthTest); }

        public static bool sleep = true; // set to false if timing is important, uses more CPU
        private static Thread thread;
        private static bool threadShouldRun = true;
        private static Queue<Entry> queue;
        private static object lockObject;

        static AsyncDebug()
        {
            lockObject = new object();
            queue = new Queue<Entry>();
            EditorApplication.update += EditorUpdate;
            thread = new Thread(Update);
            thread.Start();
        }

        private static void EditorUpdate()
        {
            if (!Application.isPlaying) threadShouldRun = false; // kill thread if Play is stopped
        }

        private static void Update()
        {
            while (threadShouldRun)
            {
                lock (lockObject)
                {
                    while (queue.Count > 0)
                    {
                        Entry entry = queue.Dequeue();
                        switch (entry.mode)
                        {
                            case Mode.Normal:
                                if (entry.context != null) Debug.Log(entry.obj, entry.context);
                                else Debug.Log(entry.obj);
                                //System.Console.WriteLine(entry.obj);
                                break;
                            case Mode.Warning:
                                if (entry.context != null) Debug.LogWarning(entry.obj, entry.context);
                                else Debug.LogWarning(entry.obj);
                                break;
                            case Mode.Exception:
                                if (entry.context != null) Debug.LogException(entry.obj as Exception, entry.context);
                                else Debug.LogException(entry.obj as Exception);
                                break;
                            case Mode.Error:
                                if (entry.context != null) Debug.LogError(entry.obj, entry.context);
                                else Debug.LogError(entry.obj);
                                break;
                        }
                    }
                }
                if (sleep) Thread.Sleep(100); // sleep a little to avoid hammering the CPU
            }
        }

        public static void Log(object obj)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Normal
                });
            }
        }
        public static void Log(object obj, Object context)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Normal,
                    context = context
                });
            }
        }

        public static void LogWarning(object obj)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Warning
                });
            }
        }
        public static void LogWarning(object obj, Object context)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Warning,
                    context = context
                });
            }
        }

        public static void LogError(object obj)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Error
                });
            }
        }
        public static void LogError(object obj, Object context)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = obj,
                    mode = Mode.Error,
                    context = context
                });
            }
        }

        public static void LogException(Exception exception)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = exception,
                    mode = Mode.Exception
                });
            }
        }
        public static void LogException(Exception exception, Object context)
        {
            lock (lockObject)
            {
                queue.Enqueue(new Entry()
                {
                    obj = exception,
                    mode = Mode.Exception,
                    context = context
                });
            }
        }

        public static void LogErrorFormat(string format, params object[] args) { Debug.LogErrorFormat(format, args); }
        public static void LogErrorFormat(Object context, string format, params object[] args) { Debug.LogErrorFormat(context, format, args); }
        public static void LogFormat(string format, params object[] args) { Debug.LogFormat(format, args); }
        public static void LogFormat(Object context, string format, params object[] args) { Debug.LogFormat(context, format, args); }
        public static void LogWarningFormat(string format, params object[] args) { Debug.LogWarningFormat(format, args); }
        public static void LogWarningFormat(Object context, string format, params object[] args) { Debug.LogWarningFormat(context, format, args); }


        private struct Entry
        {
            public object obj;
            public Mode mode;
            public Object context;
        }

        private enum Mode
        {
            Normal,
            Warning,
            Error,
            Exception
        }
    }   
} 

//#endif