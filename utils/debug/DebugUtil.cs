﻿using System.IO;
using System.Text;
using UnityEngine;

namespace unity_common_lib.utils.debug
{
    public class DebugUtil
    {
        public static string LOG_FILE_NAME = "unity_log.txt";

        public static bool ASYNC_LOG_TO_EDITOR = true;
        public static bool LOG_TO_EDITOR = true;
        public static bool LOG_TO_FILE = true;
        public static bool LOG_TO_CONSOLE = true;


        static DebugUtil()
        {
            clearOldLogFile();
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public static void Log(object obj, Object context = null)
        {
            if (LOG_TO_EDITOR)
                logToEditor(obj, context);

            if (LOG_TO_FILE)
                logToFile(obj, context);

            if (LOG_TO_CONSOLE)
                logToConsole(obj, context);
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private static void logToEditor(object obj, Object context = null)
        {
            if (ASYNC_LOG_TO_EDITOR)
                AsyncDebug.Log(obj, context);
            else
            {
                if (context == null)
                    Debug.Log(obj);
                else
                    Debug.Log(obj, context);
            }
        }

        private static void logToFile(object obj, Object context = null)
        {
            string filePath = Application.persistentDataPath + "\\" + LOG_FILE_NAME;
            StreamWriter streamWriter = new StreamWriter(filePath, true, Encoding.UTF8);

            streamWriter.WriteLine(obj);
            //OR
            //streamWriter.WriteLine("{0} some text {1}", lineOfText, lineOfText);

            streamWriter.Close();
        }

        private static void logToConsole(object obj, Object context = null)
        {
            // TODO: implement
        }

        private static void clearOldLogFile()
        {
            string filePath = Application.persistentDataPath + "\\" + LOG_FILE_NAME;

            if (File.Exists(filePath))
                File.Delete(filePath);
        }
    }
}