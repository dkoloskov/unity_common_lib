﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace unity_common_lib.utils
{
    public class UnityUtils
    {
        private static List<Action> executeInNextFrameActions;
        private static Action nextFrameAction;
        private static bool executeInNextFrame;

        static UnityUtils()
        {
            executeInNextFrameActions = new List<Action>();
            nextFrameAction = new Action(nextFrame);
            executeInNextFrame = false;
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        // ++++++++++++++++++++++++++++ EVENT TRIGGERS ++++++++++++++++++++++++++++
        public static void AddListenerPointerEnter(GameObject gameObject, Action<PointerEventData> callback)
        {
            UnityAction<BaseEventData> callbackWrapper = (eventData) => { callback((PointerEventData) eventData); };
            AddEventTriggerListener(gameObject, EventTriggerType.PointerEnter, callbackWrapper);
        }

        public static void AddListenerPointerExit(GameObject gameObject, Action<PointerEventData> callback)
        {
            UnityAction<BaseEventData> callbackWrapper = (eventData) => { callback((PointerEventData) eventData); };
            AddEventTriggerListener(gameObject, EventTriggerType.PointerExit, callbackWrapper);
        }

        public static void AddListenerPointerDown(GameObject gameObject, Action<PointerEventData> callback)
        {
            UnityAction<BaseEventData> callbackWrapper = (eventData) => { callback((PointerEventData) eventData); };
            AddEventTriggerListener(gameObject, EventTriggerType.PointerDown, callbackWrapper);
        }

        public static void AddListenerPointerUp(GameObject gameObject, Action<PointerEventData> callback)
        {
            UnityAction<BaseEventData> callbackWrapper = (eventData) => { callback((PointerEventData) eventData); };
            AddEventTriggerListener(gameObject, EventTriggerType.PointerUp, callbackWrapper);
        }
        // ---------------------------- EVENT TRIGGERS ----------------------------

        public static void SetAlpha(GameObject gameObject, float value)
        {
            SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
            Color color = renderer.color;
            color.a = value;
            renderer.color = color;
        }

        // TODO: Think how texture will be removed after game object removed.
        // TODO: Try to save 1 texture, size and color of which will be changed on next function call.
        public static GameObject DrawRect(string name, Rect r, Color color, bool colider2d = false)
        {
            GameObject rectangle = new GameObject(name);
            rectangle.AddComponent<SpriteRenderer>();

            Texture2D texture = Create2DTexture((int) r.width, (int) r.height, color);
            // Align to bottom left corner
            // TODO: Move 1 - pixelsPerUnit to config
            Sprite sprite = Sprite.Create(texture, r, Vector2.zero, 1);
            rectangle.GetComponent<SpriteRenderer>().sprite = sprite;

            if (colider2d)
            {
                BoxCollider2D boxCollider2D = rectangle.AddComponent<BoxCollider2D>();
                boxCollider2D.size = new Vector2(texture.width, texture.height);
            }
            return rectangle;
        }

        public static Texture2D Create2DTexture(int width, int height, Color color)
        {
            Texture2D texture = new Texture2D(width, height);
            Color[] fillColorArray = texture.GetPixels();
            for (var i = 0; i < fillColorArray.Length; ++i)
            {
                fillColorArray[i] = color;
            }

            texture.SetPixels(fillColorArray);
            texture.Apply();

            return texture;
        }

        public static void ExecuteInNextFrame(Action action)
        {
            executeInNextFrameActions.Add(action);
            if (!executeInNextFrame)
            {
                executeInNextFrame = true;
                UnityGlobal.AddUpdateHandler(nextFrameAction);
            }
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private static void AddEventTriggerListener(GameObject gameObject, EventTriggerType eventType,
            UnityAction<BaseEventData> callbackWrapper)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventType;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener(callbackWrapper);

            EventTrigger eventTrigger = gameObject.GetComponent<EventTrigger>();
            eventTrigger.triggers.Add(entry);
        }

        private static void nextFrame()
        {
            executeInNextFrame = false;
            UnityGlobal.RemoveUpdateHandler(nextFrameAction);

            while (executeInNextFrameActions.Count > 0)
            {
                Action action = executeInNextFrameActions[0];
                executeInNextFrameActions.RemoveAt(0);
                action();
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public static bool PointerDown
        {
            get
            {
                bool result = Input.GetMouseButton(0) || (Input.touchCount > 0);
                return result;
            }
        }
    }
}